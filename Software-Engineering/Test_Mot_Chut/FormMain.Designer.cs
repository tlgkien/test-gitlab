﻿namespace Test_Mot_Chut
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.pnlContain = new System.Windows.Forms.Panel();
            this.buttonCNCSDL = new System.Windows.Forms.Button();
            this.lbVaccine = new System.Windows.Forms.Label();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnLichHen = new System.Windows.Forms.Button();
            this.btnPhieuKham = new System.Windows.Forms.Button();
            this.btnNhanVien = new System.Windows.Forms.Button();
            this.btnKhachHang = new System.Windows.Forms.Button();
            this.btnKhoThuoc = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pnlControl = new System.Windows.Forms.Panel();
            this.iconHide = new System.Windows.Forms.PictureBox();
            this.iconExit = new System.Windows.Forms.PictureBox();
            this.btnSlide = new System.Windows.Forms.PictureBox();
            this.pnlMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pnlControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconHide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSlide)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlContain
            // 
            this.pnlContain.Location = new System.Drawing.Point(170, 31);
            this.pnlContain.Name = "pnlContain";
            this.pnlContain.Size = new System.Drawing.Size(1192, 703);
            this.pnlContain.TabIndex = 3;
            // 
            // buttonCNCSDL
            // 
            this.buttonCNCSDL.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonCNCSDL.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.buttonCNCSDL.FlatAppearance.BorderSize = 0;
            this.buttonCNCSDL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCNCSDL.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCNCSDL.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonCNCSDL.Image = global::Test_Mot_Chut.Properties.Resources.refresh__2_;
            this.buttonCNCSDL.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCNCSDL.Location = new System.Drawing.Point(1, 614);
            this.buttonCNCSDL.Name = "buttonCNCSDL";
            this.buttonCNCSDL.Size = new System.Drawing.Size(170, 80);
            this.buttonCNCSDL.TabIndex = 7;
            this.buttonCNCSDL.Text = "      Cập nhật ";
            this.buttonCNCSDL.UseVisualStyleBackColor = true;
            this.buttonCNCSDL.Click += new System.EventHandler(this.buttonCNCSDL_Click);
            // 
            // lbVaccine
            // 
            this.lbVaccine.AutoSize = true;
            this.lbVaccine.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbVaccine.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lbVaccine.Font = new System.Drawing.Font("Bahnschrift SemiLight SemiConde", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVaccine.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbVaccine.Location = new System.Drawing.Point(45, 20);
            this.lbVaccine.Name = "lbVaccine";
            this.lbVaccine.Size = new System.Drawing.Size(122, 39);
            this.lbVaccine.TabIndex = 2;
            this.lbVaccine.Text = "VACCINE";
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.SystemColors.Highlight;
            this.pnlMenu.Controls.Add(this.btnNhanVien);
            this.pnlMenu.Controls.Add(this.btnKhoThuoc);
            this.pnlMenu.Controls.Add(this.buttonCNCSDL);
            this.pnlMenu.Controls.Add(this.btnLichHen);
            this.pnlMenu.Controls.Add(this.btnPhieuKham);
            this.pnlMenu.Controls.Add(this.btnKhachHang);
            this.pnlMenu.Controls.Add(this.lbVaccine);
            this.pnlMenu.Controls.Add(this.pictureBox2);
            this.pnlMenu.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(170, 734);
            this.pnlMenu.TabIndex = 0;
            // 
            // btnLichHen
            // 
            this.btnLichHen.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnLichHen.FlatAppearance.BorderSize = 0;
            this.btnLichHen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLichHen.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLichHen.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLichHen.Image = ((System.Drawing.Image)(resources.GetObject("btnLichHen.Image")));
            this.btnLichHen.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLichHen.Location = new System.Drawing.Point(0, 512);
            this.btnLichHen.Name = "btnLichHen";
            this.btnLichHen.Size = new System.Drawing.Size(170, 80);
            this.btnLichHen.TabIndex = 6;
            this.btnLichHen.Text = "    Lịch Hẹn";
            this.btnLichHen.UseVisualStyleBackColor = true;
            this.btnLichHen.Click += new System.EventHandler(this.btnLichHen_Click);
            // 
            // btnPhieuKham
            // 
            this.btnPhieuKham.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnPhieuKham.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnPhieuKham.FlatAppearance.BorderSize = 0;
            this.btnPhieuKham.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPhieuKham.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhieuKham.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPhieuKham.Image = ((System.Drawing.Image)(resources.GetObject("btnPhieuKham.Image")));
            this.btnPhieuKham.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPhieuKham.Location = new System.Drawing.Point(1, 308);
            this.btnPhieuKham.Name = "btnPhieuKham";
            this.btnPhieuKham.Size = new System.Drawing.Size(170, 80);
            this.btnPhieuKham.TabIndex = 5;
            this.btnPhieuKham.Text = "     Phiếu Khám";
            this.btnPhieuKham.UseVisualStyleBackColor = false;
            this.btnPhieuKham.Click += new System.EventHandler(this.btnPhieuKham_Click);
            // 
            // btnNhanVien
            // 
            this.btnNhanVien.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnNhanVien.FlatAppearance.BorderSize = 0;
            this.btnNhanVien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNhanVien.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNhanVien.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnNhanVien.Image = ((System.Drawing.Image)(resources.GetObject("btnNhanVien.Image")));
            this.btnNhanVien.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNhanVien.Location = new System.Drawing.Point(0, 206);
            this.btnNhanVien.Name = "btnNhanVien";
            this.btnNhanVien.Size = new System.Drawing.Size(170, 80);
            this.btnNhanVien.TabIndex = 4;
            this.btnNhanVien.Text = "    Nhân Viên";
            this.btnNhanVien.UseVisualStyleBackColor = true;
            this.btnNhanVien.Click += new System.EventHandler(this.btnNhanVien_Click);
            // 
            // btnKhachHang
            // 
            this.btnKhachHang.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnKhachHang.FlatAppearance.BorderSize = 0;
            this.btnKhachHang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKhachHang.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhachHang.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnKhachHang.Image = ((System.Drawing.Image)(resources.GetObject("btnKhachHang.Image")));
            this.btnKhachHang.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKhachHang.Location = new System.Drawing.Point(0, 410);
            this.btnKhachHang.Name = "btnKhachHang";
            this.btnKhachHang.Size = new System.Drawing.Size(170, 80);
            this.btnKhachHang.TabIndex = 3;
            this.btnKhachHang.Text = "Khách Hàng";
            this.btnKhachHang.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnKhachHang.UseVisualStyleBackColor = true;
            this.btnKhachHang.Click += new System.EventHandler(this.btnKhachHang_Click);
            // 
            // btnKhoThuoc
            // 
            this.btnKhoThuoc.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnKhoThuoc.FlatAppearance.BorderSize = 0;
            this.btnKhoThuoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKhoThuoc.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhoThuoc.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnKhoThuoc.Image = ((System.Drawing.Image)(resources.GetObject("btnKhoThuoc.Image")));
            this.btnKhoThuoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKhoThuoc.Location = new System.Drawing.Point(0, 104);
            this.btnKhoThuoc.Name = "btnKhoThuoc";
            this.btnKhoThuoc.Size = new System.Drawing.Size(170, 80);
            this.btnKhoThuoc.TabIndex = 2;
            this.btnKhoThuoc.Text = "    Kho Thuốc";
            this.btnKhoThuoc.UseVisualStyleBackColor = true;
            this.btnKhoThuoc.Click += new System.EventHandler(this.btnKhoThuoc_Click_1);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(57, 102);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pnlControl
            // 
            this.pnlControl.Controls.Add(this.iconHide);
            this.pnlControl.Controls.Add(this.iconExit);
            this.pnlControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlControl.Location = new System.Drawing.Point(0, 0);
            this.pnlControl.Name = "pnlControl";
            this.pnlControl.Size = new System.Drawing.Size(1362, 32);
            this.pnlControl.TabIndex = 1;
            // 
            // iconHide
            // 
            this.iconHide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconHide.Cursor = System.Windows.Forms.Cursors.Hand;
            this.iconHide.Image = ((System.Drawing.Image)(resources.GetObject("iconHide.Image")));
            this.iconHide.Location = new System.Drawing.Point(1313, 3);
            this.iconHide.Name = "iconHide";
            this.iconHide.Size = new System.Drawing.Size(20, 22);
            this.iconHide.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.iconHide.TabIndex = 5;
            this.iconHide.TabStop = false;
            this.iconHide.Click += new System.EventHandler(this.iconHide_Click);
            // 
            // iconExit
            // 
            this.iconExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.iconExit.Image = ((System.Drawing.Image)(resources.GetObject("iconExit.Image")));
            this.iconExit.Location = new System.Drawing.Point(1339, 3);
            this.iconExit.Name = "iconExit";
            this.iconExit.Size = new System.Drawing.Size(20, 22);
            this.iconExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.iconExit.TabIndex = 2;
            this.iconExit.TabStop = false;
            this.iconExit.Click += new System.EventHandler(this.iconExit_Click);
            // 
            // btnSlide
            // 
            this.btnSlide.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSlide.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSlide.Image = ((System.Drawing.Image)(resources.GetObject("btnSlide.Image")));
            this.btnSlide.Location = new System.Drawing.Point(170, 0);
            this.btnSlide.Name = "btnSlide";
            this.btnSlide.Size = new System.Drawing.Size(32, 32);
            this.btnSlide.TabIndex = 4;
            this.btnSlide.TabStop = false;
            this.btnSlide.Click += new System.EventHandler(this.btnSlide_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 734);
            this.Controls.Add(this.pnlContain);
            this.Controls.Add(this.pnlMenu);
            this.Controls.Add(this.btnSlide);
            this.Controls.Add(this.pnlControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormMain";
            this.Text = "FormMain";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.pnlMenu.ResumeLayout(false);
            this.pnlMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pnlControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iconHide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSlide)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlContain;
        private System.Windows.Forms.Button btnKhachHang;
        private System.Windows.Forms.Button btnKhoThuoc;
        private System.Windows.Forms.Label lbVaccine;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnLichHen;
        private System.Windows.Forms.Button btnPhieuKham;
        private System.Windows.Forms.Button btnNhanVien;
        private System.Windows.Forms.PictureBox iconHide;
        private System.Windows.Forms.PictureBox iconExit;
        private System.Windows.Forms.PictureBox btnSlide;
        private System.Windows.Forms.Panel pnlControl;
        private System.Windows.Forms.Button buttonCNCSDL;
    }
}