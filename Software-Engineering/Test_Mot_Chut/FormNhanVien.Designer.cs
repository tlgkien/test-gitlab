﻿namespace Test_Mot_Chut
{
    partial class FormNhanVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.textBoxTimkiem = new System.Windows.Forms.TextBox();
            this.buttonTimkiem = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewNhanVien = new System.Windows.Forms.DataGridView();
            this.buttonthemmoi = new System.Windows.Forms.Button();
            this.buttonTHONGKE = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNhanVien)).BeginInit();
            this.SuspendLayout();
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(1160, 549);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(97, 21);
            this.radioButton2.TabIndex = 14;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "ĐÃ BỊ XÓA";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(941, 549);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(135, 21);
            this.radioButton1.TabIndex = 13;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "ĐANG LÀM VIỆC";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // textBoxTimkiem
            // 
            this.textBoxTimkiem.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTimkiem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.textBoxTimkiem.Location = new System.Drawing.Point(407, 30);
            this.textBoxTimkiem.Name = "textBoxTimkiem";
            this.textBoxTimkiem.Size = new System.Drawing.Size(305, 34);
            this.textBoxTimkiem.TabIndex = 11;
            // 
            // buttonTimkiem
            // 
            this.buttonTimkiem.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonTimkiem.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.buttonTimkiem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.buttonTimkiem.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTimkiem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonTimkiem.Location = new System.Drawing.Point(763, 22);
            this.buttonTimkiem.Name = "buttonTimkiem";
            this.buttonTimkiem.Size = new System.Drawing.Size(165, 51);
            this.buttonTimkiem.TabIndex = 10;
            this.buttonTimkiem.Text = "TÌM KIẾM";
            this.buttonTimkiem.UseVisualStyleBackColor = false;
            this.buttonTimkiem.Click += new System.EventHandler(this.buttonTimkiem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label1.Location = new System.Drawing.Point(267, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "TỪ KHÓA";
            // 
            // dataGridViewNhanVien
            // 
            this.dataGridViewNhanVien.AllowUserToAddRows = false;
            this.dataGridViewNhanVien.AllowUserToDeleteRows = false;
            this.dataGridViewNhanVien.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewNhanVien.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewNhanVien.Location = new System.Drawing.Point(25, 110);
            this.dataGridViewNhanVien.MultiSelect = false;
            this.dataGridViewNhanVien.Name = "dataGridViewNhanVien";
            this.dataGridViewNhanVien.ReadOnly = true;
            this.dataGridViewNhanVien.RowHeadersWidth = 51;
            this.dataGridViewNhanVien.RowTemplate.Height = 24;
            this.dataGridViewNhanVien.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewNhanVien.Size = new System.Drawing.Size(1384, 419);
            this.dataGridViewNhanVien.TabIndex = 8;
            this.dataGridViewNhanVien.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewNhanVien_CellClick);
            // 
            // buttonthemmoi
            // 
            this.buttonthemmoi.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonthemmoi.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.buttonthemmoi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.buttonthemmoi.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonthemmoi.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonthemmoi.Location = new System.Drawing.Point(944, 22);
            this.buttonthemmoi.Name = "buttonthemmoi";
            this.buttonthemmoi.Size = new System.Drawing.Size(165, 51);
            this.buttonthemmoi.TabIndex = 15;
            this.buttonthemmoi.Text = "THÊM MỚI";
            this.buttonthemmoi.UseVisualStyleBackColor = false;
            this.buttonthemmoi.Click += new System.EventHandler(this.buttonthemmoi_Click);
            // 
            // buttonTHONGKE
            // 
            this.buttonTHONGKE.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonTHONGKE.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.buttonTHONGKE.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.buttonTHONGKE.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTHONGKE.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonTHONGKE.Location = new System.Drawing.Point(1125, 22);
            this.buttonTHONGKE.Name = "buttonTHONGKE";
            this.buttonTHONGKE.Size = new System.Drawing.Size(165, 51);
            this.buttonTHONGKE.TabIndex = 16;
            this.buttonTHONGKE.Text = "THỐNG KÊ";
            this.buttonTHONGKE.UseVisualStyleBackColor = false;
            this.buttonTHONGKE.Click += new System.EventHandler(this.buttonTHONGKE_Click);
            // 
            // FormNhanVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(1436, 601);
            this.Controls.Add(this.buttonTHONGKE);
            this.Controls.Add(this.buttonthemmoi);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.textBoxTimkiem);
            this.Controls.Add(this.buttonTimkiem);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewNhanVien);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "FormNhanVien";
            this.Text = "7";
            this.Load += new System.EventHandler(this.FormNhanVien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNhanVien)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TextBox textBoxTimkiem;
        private System.Windows.Forms.Button buttonTimkiem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewNhanVien;
        private System.Windows.Forms.Button buttonthemmoi;
        private System.Windows.Forms.Button buttonTHONGKE;
    }
}